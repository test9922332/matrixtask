package com.java.constants;

public class MessageConstants {

    public static final String MENU = """
        Please, select and enter operation:
        
        1 - print Snake matrix
        2 - print Pyramid matrix
        3 - Exit
        """;
    public static final String UNKNOWN_OPERATION = "Unknown operation!";

    public static final String FAREWELL = "Have a good day!";
    public static final String DEFINE_MATRIX_SIZE = """
                    
        Please, enter the matrix size
        """;
    public static final String TWO_DIGIT_FORMAT = "%02d";
    public static final String THREE_DIGIT_FORMAT = "%03d";

}
