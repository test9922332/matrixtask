package com.java;

import com.java.service.impl.MenuFacade;

public class MatrixApplication {

    public static void main(String[] args) {
        new MenuFacade().initMenu();
    }
}
