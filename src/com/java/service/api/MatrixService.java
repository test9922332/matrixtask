package com.java.service.api;

public interface MatrixService {

    void fillWithFieldsMatrix(int[][] twoDimArray);

    void printMatrix(int[][] twoDimArray, String format);
}
