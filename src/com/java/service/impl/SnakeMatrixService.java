package com.java.service.impl;

import com.java.service.api.MatrixService;

public class SnakeMatrixService extends BaseMatrixService implements MatrixService {

    @Override
    public void fillWithFieldsMatrix(int[][] matrix) {
        int currentNumber = 1;
        int firstRowIndex = 0, firstColumnIndex = 0;
        int lastColumnIndex = matrix.length - 1, lastRowIndex = matrix.length - 1;
        while (firstRowIndex <= matrix.length / 2
            && lastColumnIndex >= matrix.length / 2) {
            currentNumber = fillFieldsOnPerimeter(
                lastColumnIndex,
                currentNumber,
                matrix,
                firstRowIndex,
                lastRowIndex,
                firstColumnIndex
            );
            firstRowIndex = firstColumnIndex += 1;
            lastColumnIndex = lastRowIndex -= 1;
        }
    }

    private int fillFieldsOnPerimeter(
        int lastColumnIndex,
        int currentNumber,
        int[][] matrix,
        int firstRowIndex,
        int lastRowIndex,
        int firstColumnIndex
    ) {
        for (int index = firstRowIndex; index < lastColumnIndex + 1; index++) {
            if (matrix[firstRowIndex][index] == 0) {
                matrix[firstRowIndex][index] = currentNumber;
                currentNumber++;
            }
        }
        for (int index = firstColumnIndex; index < lastRowIndex + 1; index++) {
            if (matrix[index][lastRowIndex] == 0) {
                matrix[index][lastRowIndex] = currentNumber;
                currentNumber++;
            }
        }
        for (int index = lastColumnIndex; index >= 0; index--) {
            if (matrix[lastColumnIndex][index] == 0) {
                matrix[lastColumnIndex][index] = currentNumber;
                currentNumber++;
            }
        }
        for (int index = lastRowIndex; index >= 0; index--) {
            if (matrix[index][firstColumnIndex] == 0) {
                matrix[index][firstColumnIndex] = currentNumber;
                currentNumber++;
            }
        }
        return currentNumber;
    }
}
