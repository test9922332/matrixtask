package com.java.service.impl;

import java.util.Arrays;
import java.util.stream.Collectors;

public class BaseMatrixService {

    public void printMatrix(int[][] matrix, String numberFormat) {
        for (int[] intArray : matrix) {
            var printedNumber = Arrays.stream(intArray)
                .mapToObj(item -> String.format(numberFormat, item))
                .collect(Collectors.joining(","));
            System.out.println(printedNumber);
        }
    }
}
