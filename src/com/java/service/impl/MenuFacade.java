package com.java.service.impl;

import static com.java.constants.MessageConstants.DEFINE_MATRIX_SIZE;
import static com.java.constants.MessageConstants.FAREWELL;
import static com.java.constants.MessageConstants.MENU;
import static com.java.constants.MessageConstants.THREE_DIGIT_FORMAT;
import static com.java.constants.MessageConstants.TWO_DIGIT_FORMAT;
import static com.java.constants.MessageConstants.UNKNOWN_OPERATION;

import com.java.service.api.MatrixService;
import java.util.Scanner;

public class MenuFacade {

    public void initMenu() {
        var scanner = new Scanner(System.in);
        selectOperation(scanner);
        scanner.close();
    }

    private void selectOperation(Scanner scanner) {
        System.out.println(MENU);
        var operation = scanner.next();
        switch (operation) {
            case "1" -> handleMatrix(scanner, new SnakeMatrixService(), THREE_DIGIT_FORMAT);
            case "2" -> handleMatrix(scanner, new PyramidMatrixService(), TWO_DIGIT_FORMAT);
            case "3" -> System.out.println(FAREWELL);
            default -> {
                System.out.println(UNKNOWN_OPERATION);
                selectOperation(scanner);
            }
        }
    }

    private void handleMatrix(
        Scanner scanner,
        MatrixService matrixService,
        String numberFormat
    ) {
        System.out.println(DEFINE_MATRIX_SIZE);
        int matrixSize = scanner.nextInt();
        int[][] matrix = new int[matrixSize][matrixSize];
        matrixService.fillWithFieldsMatrix(matrix);
        matrixService.printMatrix(matrix, numberFormat);
        System.out.println(); // Empty line to separate text
        selectOperation(scanner);
    }
}
