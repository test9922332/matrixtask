package com.java.service.impl;

import com.java.service.api.MatrixService;

public class PyramidMatrixService extends BaseMatrixService implements MatrixService {

    @Override
    public void fillWithFieldsMatrix(int[][] matrix) {
        int currentNumber = 1;
        int firstRowIndex = 0, firstColumnIndex = 0;
        int lastColumnIndex = matrix.length - 1, lastRowIndex = matrix.length - 1;
        while (firstRowIndex <= matrix.length / 2
            && lastColumnIndex >= matrix.length / 2) {
            currentNumber = fillFieldsOnPerimeter(
                matrix,
                currentNumber,
                firstRowIndex,
                lastColumnIndex,
                lastRowIndex,
                firstColumnIndex
            );
            firstRowIndex = firstColumnIndex += 1;
            lastColumnIndex = lastRowIndex -= 1;
        }
    }

    private static int fillFieldsOnPerimeter(int[][] matrix, int currentNumber,
        int firstRowIndex, int lastColumnIndex, int lastRowIndex, int firstColumnIndex) {
        for (int index = firstRowIndex; index < lastColumnIndex + 1; index++) {
            if (matrix[firstRowIndex][index] == 0 && matrix[lastRowIndex][index] == 0) {
                matrix[firstRowIndex][index] = currentNumber;
                matrix[lastRowIndex][index] = currentNumber;
            }
        }
        for (int index = firstColumnIndex; index < lastRowIndex + 1; index++) {
            if (matrix[index][firstRowIndex] == 0 && matrix[index][lastRowIndex] == 0) {
                matrix[index][firstRowIndex] = currentNumber;
                matrix[index][lastRowIndex] = currentNumber;
            }
        }
        currentNumber++;
        return currentNumber;
    }

}
